const colors = {
    $brand200: '#d5feff',
    $brand500: '#00e1e4',
    $brand600: '#00d2d4',
    $red500: '#ff4e4e',
    $gray0: '#ffffff',
    $gray50: '#f7f7f7',
    $gray100: '#eeeeee',
    $gray150: '#e4e4e4',
    $gray300: '#c4c4c4',
    $gray400: '#ababab',
    $gray800: '#505050',
    $gray900: '#2f2f2f',
};

export default colors;