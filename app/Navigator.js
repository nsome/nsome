import {
    createAppContainer,
    createSwitchNavigator, NavigationActions
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {
    createBottomTabNavigator,
    createMaterialTopTabNavigator
} from 'react-navigation-tabs';
import {
    View
} from 'react-native';
import * as ScreenNames from './screens/ScreenNames';
import { SplashScreen } from './screens/Splash';
import { LoginScreen } from './screens/Login';
import {
    MyProfileScreen,
    OtherProfileScreen,
} from './screens/Profile';
import {
    AwaitingAcceptanceScreen,
    ChatScreen,
    ChatRoomScreen
} from './screens/Chat';
import {
    PhoneAuthScreen,
    SexSelectionScreen
} from './screens/SignUp';
import {
    LoungeScreen,
    MatchingScreen,
    QuizScreen,
    RatingScreen,
} from './screens/Main';
import {
    BeanUsageScreen,
    StoreScreen
} from './screens/Store';
import colors from './colors';
import {
    MainHeaderLeft,
    MainHeaderRight,
    HeaderReport,
    HeaderClose,
} from './components'
import React from 'react';
import { Image } from 'react-native';
import {
    AvoidAcquaintanceScreen,
    EventScreen,
    InquiryScreen,
    MoreScreen,
    NoticeScreen,
    SettingScreen,
    TermsAndConditionsScreen,
    TermsOfUseScreen,
    PrivacyPolicyScreen,
} from './screens/More';


const SignUpStack = createStackNavigator({
    [ScreenNames.SIGN_UP_PHONE_AUTH]: PhoneAuthScreen,
    [ScreenNames.SIGN_UP_SEX_SELECTION]: SexSelectionScreen
});

const LoungeStack = createStackNavigator({
    [ScreenNames.MAIN_LOUNGE]: LoungeScreen,
}, {
    initialRouteName: ScreenNames.MAIN_LOUNGE,
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
        headerStyle: {
            height: 56,
            borderBottomWidth: 0
        },
        headerTitleStyle: {
            color: colors.$gray800,
            fontSize: 18
        },
        headerLeft: (
            <MainHeaderLeft
                beanNum={10}
            />
        ),
        headerRight: (
            <MainHeaderRight />
        )
    }
});

export const OtherProfileStack = createStackNavigator({
    [ScreenNames.OTHER_PROFILE]: {
        screen: OtherProfileScreen
    },
}, {
    headerMode: 'screen',
    headerLayoutPreset: 'center',
    initialRouteName: [ScreenNames.OTHER_PROFILE],
    defaultNavigationOptions: ({ navigation }) => ({
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            top: 0,
            left: 0,
            right: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
            elevation: 0,
        },
        headerTransparent: true,
        headerLeft: (
            <HeaderReport />
        ),
        headerRight: (
            <HeaderClose
                white={true}
                onPress={() => navigation.dispatch(NavigationActions.back())}
            />
        )
    })
});

const MatchingStack = createStackNavigator({
    [ScreenNames.MAIN_MATCHING]: MatchingScreen,
}, {
    initialRouteName: ScreenNames.MAIN_MATCHING,
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
        title: '오늘의 매칭',
        headerStyle: {
            height: 56,
            borderBottomWidth: 0,
            elevation: 0,
        },
        headerTitleStyle: {
            color: colors.$gray800,
            fontSize: 18,
        },
        headerLeft: (
            <MainHeaderLeft
                beanNum={10}
            />
        ),
        headerRight: (
            <MainHeaderRight />
        )
    }
});

const QuizStack = createStackNavigator({
    [ScreenNames.MAIN_QUIZ]: QuizScreen,
}, {
    initialRouteName: ScreenNames.MAIN_QUIZ,
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
        headerLayoutPreset: 'center',
        headerStyle: {
            height: 56,
            borderBottomWidth: 0,
            elevation: 0,
        },
        headerTitleStyle: {
            color: colors.$gray800,
            fontSize: 18
        },
        headerLeft: (
            <MainHeaderLeft
                beanNum={10}
            />
        ),
        headerRight: (
            <MainHeaderRight />
        )
    }
});

const RatingStack = createStackNavigator({
    [ScreenNames.MAIN_RATING]: RatingScreen,
}, {
    initialRouteName: ScreenNames.MAIN_RATING,
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
        headerStyle: {
            height: 56,
            borderBottomWidth: 0,
            elevation: 0,
        },
        headerTitleStyle: {
            color: colors.$gray800,
            fontSize: 18
        },
        headerLeft: (
            <MainHeaderLeft
                beanNum={10}
            />
        ),
        headerRight: (
            <MainHeaderRight />
        )
    }
});


const TermsAndConditionsTopTab = createMaterialTopTabNavigator({
    [ScreenNames.TERMS_OF_USE]: {
        screen: TermsOfUseScreen,
        navigationOptions: {
            tabBarLabel: "이용 약관"
        }
    },
    [ScreenNames.PRIVACY_POLICY]: {
        screen: PrivacyPolicyScreen,
        navigationOptions: {
            tabBarLabel: "개인정보처리방침"
        }
    }
}, {
    navigationOptions: {
        title: "이용 약관",
    },
    defaultNavigationOptions: {
        headerStyle: {
            height: 56,
            borderBottomWidth: 0,
            elevation: 0,
        },
        headerTitleStyle: {
            color: colors.$gray800,
            fontSize: 18
        },
    },
    tabBarOptions: {
        activeTintColor: colors.$brand500,
        inactiveTintColor: colors.$gray800,
        labelStyle: {
            fontSize: 14,
            justifyContent: 'center',
            alignSelf: 'center',
        },
        style: {
            height: 40,
            backgroundColor: colors.$gray0
        },
        indicatorStyle: {
            borderBottomWidth: 1,
            borderColor: colors.$brand500,
            backgroundColor: colors.$gray0
        }
    }
});

const MoreStack = createStackNavigator({
    [ScreenNames.BEAN_USAGE]: BeanUsageScreen,
    [ScreenNames.AVOID_ACQUAINTANCE]: AvoidAcquaintanceScreen,
    [ScreenNames.EVENT]: EventScreen,
    [ScreenNames.INQUIRY]: InquiryScreen,
    [ScreenNames.MORE]: MoreScreen,
    [ScreenNames.NOTICE]: NoticeScreen,
    [ScreenNames.SETTING]: SettingScreen,
    [ScreenNames.TERMS_AND_CONDITIONS]: TermsAndConditionsTopTab,
}, {
    initialRouteName: ScreenNames.MORE,
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
        headerStyle: {
            height: 56,
            borderBottomWidth: 0,
            elevation: 0,
        },
        headerTitleStyle: {
            color: colors.$gray800,
            fontSize: 18
        },
    }
});

MoreStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }

    return {
        tabBarVisible
    };
};

const MainBottomTab = createBottomTabNavigator(
    {
        [ScreenNames.MAIN_MATCHING]: MatchingStack,
        [ScreenNames.MAIN_QUIZ]: QuizStack,
        [ScreenNames.MAIN_LOUNGE]: LoungeStack,
        [ScreenNames.MAIN_RATING]: RatingStack,
        [ScreenNames.MORE]: MoreStack
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused }) => {
                const { routeName } = navigation.state;
                let iconSrc;
                switch (routeName) {
                    case ScreenNames.MAIN_MATCHING:
                        iconSrc = focused
                            ? require('./images/tabicon_card_on.png')
                            : require('./images/tabicon_card_off.png');
                        break;
                    case ScreenNames.MAIN_QUIZ:
                        iconSrc = focused
                            ? require('./images/tabicon_quiz_on.png')
                            : require('./images/tabicon_quiz_off.png');
                        break;
                    case ScreenNames.MAIN_LOUNGE:
                        iconSrc = focused
                            ? require('./images/tabicon_lounge_on.png')
                            : require('./images/tabicon_lounge_off.png');
                        break;
                    case ScreenNames.MAIN_RATING:
                        iconSrc = focused
                            ? require('./images/tabicon_rate_on.png')
                            : require('./images/tabicon_rate_off.png');
                        break;
                    case ScreenNames.MORE:
                        iconSrc = focused
                            ? require('./images/tabicon_more_on.png')
                            : require('./images/tabicon_more_off.png');
                        break;
                }

                return <Image source={iconSrc} style={{ width: 32, height: 32 }} />
            }
        }),
        tabBarOptions: {
            showLabel: false,
            activeTintColor: colors.$brand500,
            inactiveTintColor: colors.$gray800,
            style: {
                borderTopColor: colors.$gray0,
                elevation: 5,
                borderTopWidth: 2,
                top: 1,
                shadowColor: '#2d3d51',
                shadowOffset: {
                    width: 0,
                    height: -4
                },
                shadowOpacity: 0.06
            }
        }
    }
);

export const StoreStack = createStackNavigator({
    [ScreenNames.STORE]: {
        screen: StoreScreen
    },
    [ScreenNames.BEAN_USAGE]: {
        screen: BeanUsageScreen
    }
}, {
    headerLayoutPreset: 'center',
    initialRouteName: [ScreenNames.STORE],
    defaultNavigationOptions: ({ navigation }) => ({
        headerStyle: {
            height: 56,
            borderBottomWidth: 1,
            borderBottomColor: colors.$gray100,
            elevation: 0,
        },
        headerTitleStyle: {
            color: colors.$gray800,
            fontSize: 18
        }
    })
});

export const ChatStack = createStackNavigator({
    [ScreenNames.CHAT]: {
        screen: ChatScreen
    },
    [ScreenNames.AWAITING_ACCEPTANCE]: AwaitingAcceptanceScreen,
    [ScreenNames.CHAT_ROOM]: ChatRoomScreen
}, {
    headerLayoutPreset: 'center',
    initialRouteName: [ScreenNames.CHAT],
    defaultNavigationOptions: ({ navigation }) => ({
        headerStyle: {
            height: 56,
            borderBottomWidth: 0,
            backgroundColor: colors.$gray50,
            borderBottomColor: colors.$gray100,
            elevation: 0,
        },
        headerTitleStyle: {
            color: colors.$gray800,
            fontSize: 18
        }
    })
});

const MainStack = createStackNavigator(
    {
        MainBottomTab,
        StoreStack: {
            screen: StoreStack
        },
        ChatStack: {
            screen: ChatStack
        },
        OtherProfileStack: {
            screen: OtherProfileStack
        }
    },
    {
        headerMode: 'none',
        mode: 'modal',
        initialRouteName: 'MainBottomTab'
    }
);

const RootSwitch = createSwitchNavigator({
    [ScreenNames.SPLASH]: SplashScreen,
    [ScreenNames.LOGIN]: LoginScreen,
    [ScreenNames.MAIN]: MainStack
}, {
    initialRouteName: ScreenNames.SPLASH
});

export default createAppContainer(RootSwitch);