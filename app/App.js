import React from 'react';
import { Dimensions } from 'react-native';
import Navigator from './Navigator';
import EStyleSheet from 'react-native-extended-stylesheet';
import colors from './colors';

const entireScreenWidth = Dimensions.get('window').width;
const BASE_WIDTH = 360;

EStyleSheet.build({
    ...colors,
    $rem: entireScreenWidth / BASE_WIDTH
});

const App = () => <Navigator/>;
export default App;