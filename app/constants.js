import { Dimensions } from 'react-native';

const window = Dimensions.get('window');

// Login
export const LOGO_HEIGHT = 110;
export const LOGO_HEIGHT_SMALL = parseInt(window.width / 6);
export const AUTH_CODE_VALIDITY_TIME = 180;