export const AUTH = require('./auth.png');
export const AUTH_FAIL = require('./auth_fail.png');
export const AUTH_SUCCESS = require('./auth_success.png');
export const LOGIN_LOGO = require('./login_logo.png');
export const PHONE = require('./phone.png');