import { Dimensions, Platform, PixelRatio } from 'react-native';

const { width, height } = Dimensions.get('window');
const BASE_WIDTH = 360;
const BASE_HEIGHT = 640;
const FACTOR = 0.5;

const horizontalScale = size => PixelRatio.roundToNearestPixel(width / BASE_WIDTH * size);
const verticalScale = size => PixelRatio.roundToNearestPixel(height / BASE_HEIGHT * size);
const moderateScale = (size, factor = FACTOR) => size + (verticalScale(size) - size) * factor;
const fontNormalize = size => {
    if (Platform.OS === 'ios') {
        return Math.round(horizontalScale(size))
    } else {
        return Math.round(horizontalScale(size)) - 2
    }
};

export {
    horizontalScale,
    verticalScale,
    moderateScale,
    fontNormalize
}