import React, { Component } from 'react';
import {
    Dimensions,
    Text,
    View
} from 'react-native';
import { MainHeaderLeft, MainHeaderRight } from '../../../components';

export default class RatingScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: "매력평가",
            headerLeft: (
                <MainHeaderLeft
                    beanNum={10}
                    navigation={navigation}
                />
            ),
            headerRight: (
                <MainHeaderRight
                    navigation={navigation}
                />
            )
        }

    };

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Text>RatingScreen</Text>
            </View>
        )
    }
}