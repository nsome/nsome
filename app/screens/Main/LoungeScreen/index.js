import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { MainHeaderLeft, MainHeaderRight } from '../../../components';

export default class LoungeScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: "라운지",
            headerLeft: (
                <MainHeaderLeft
                    beanNum={10}
                    navigation={navigation}
                />
            ),
            headerRight: (
                <MainHeaderRight
                    navigation={navigation}
                />
            )
        }

    };

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Text>LoungeScreen</Text>
            </View>
        )
    }
}