import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    viewRoot: {
        justifyContent: 'center',
        flex: 1,
        paddingTop: 16,
        paddingStart: 16,
        paddingEnd: 16,
    },
    viewTitleHeart: {
        marginBottom: 14,
        flexDirection: 'row',
        alignItems: 'center',
    },
    imgTitleHeart: {
        width: 20,
        height: 20
    },
    textTitleHeart: {
        marginStart: 2,
        fontSize: 15,
    },
    textTitleHeartToday: {
        color: '$brand500',
    },
    textTitleHeartLast: {
        color: '$gray400'
    },
    viewTodayPersonCards: {
        paddingStart: 2,
        paddingEnd: 2,
    },
    viewBtnMorePerson: {
        flexDirection: 'row',
        height: 44,
        marginStart: 2,
        marginEnd: 2,
        borderColor: '$brand500',
        borderWidth: 1,
        borderRadius: 6,
        borderStyle: 'dashed',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textMorePerson: {
        color: '$brand500',
        fontSize: 14,
    },
    viewMorePersonBean: {
        position: 'absolute',
        top: 9,
        bottom: 9,
        right: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    imgMorePersonBean: {
        width: 26,
        height: 26
    },
    textMorePersonBeanNum: {
        marginStart: 2,
        color: '$brand500',
        fontSize: 16,
    },
    viewDivider: {
        width: '100%',
        height: 1,
        backgroundColor: '$gray100',
        marginTop: 20,
    }
});

export default styles;