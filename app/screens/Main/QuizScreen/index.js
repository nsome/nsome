import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { MainHeaderLeft, MainHeaderRight } from '../../../components';

export default class QuizScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: "오늘의 공통분모",
            headerLeft: (
                <MainHeaderLeft
                    beanNum={10}
                    navigation={navigation}
                />
            ),
            headerRight: (
                <MainHeaderRight
                    navigation={navigation}
                />
            )
        }

    };

    render() {

        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Text>QuizScreen</Text>
            </View>
        )
    }
}