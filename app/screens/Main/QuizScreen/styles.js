import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    viewRoot: {
        flex: 1,
        flexDirection: 'column',
    },
    viewBeforeQuiz: {
        backgroundColor: '$gray50'
    },

});

export default styles;