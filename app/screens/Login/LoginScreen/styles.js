import React from 'react';
import {
    horizontalScale,
    moderateScale,
    verticalScale
} from '../../../utils/Scaling';
import EStyleSheet from 'react-native-extended-stylesheet';


const styles = EStyleSheet.create({
    rootLayout: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '$gray0'
    },
    logoLayout: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '42rem'
    },
    formLayout: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: '42rem'
    },
    inputLayout: {
        flexDirection: 'row',
        borderBottomColor: '$gray100',
        borderBottomWidth: 1,
        width: horizontalScale(310),
        aspectRatio: 310 / 52,
        marginBottom: '8rem',
        alignItems: 'center'
    },
    inputIcon: {
        resizeMode: 'contain',
        position: 'absolute',
        left: horizontalScale(4),
        width: moderateScale(24)
    },
    inputText: {
        width: '100%',
        paddingLeft: horizontalScale(42),
        borderBottomWidth: 0,
        fontSize: '15rem'
    },
    inputButton: {
        position: 'absolute',
        right: '4rem',
        width: horizontalScale(96),
        height: verticalScale(34),
        borderRadius: 4,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputButtonText: {
        fontSize: '14rem'
    },
    timerLayout: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    authIcon: {
        width: moderateScale(14),
        height: moderateScale(14),
        marginEnd: horizontalScale(11)
    },
    timerText: {
        color: '$gray300',
        fontSize: '15rem'
    },
    buttonLayout: {
        height: 52,
        marginTop: 22,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewSubButtons: {
        width: 310,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 42
    },
    textSubButton: {
        color: '$gray300',
        fontSize: 14
    }
});

export default styles;