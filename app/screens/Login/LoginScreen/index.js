import React, { Fragment } from 'react';
import {
    Animated,
    Image,
    Keyboard,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    View,
    StatusBar,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
import {
    FlatButton
} from '../../../components';
import styles from './styles';
import {
    AUTH_CODE_VALIDITY_TIME,
    LOGO_HEIGHT,
    LOGO_HEIGHT_SMALL
} from '../../../constants';
import colors from '../../../colors';
import * as ScreenNames from '../../ScreenNames';


class LoginScreen extends React.Component {
    static navigationOptions = {
        title: 'LoginScreen'
    };

    constructor(props) {
        super(props);
        this.state = {
            time: {
                mm: '03',
                ss: '00'
            },
            authCode: '',
            phoneNum: '',
            isAlreadySentAuthCode: false,
            isSentAuthCode: false,
            remainingTime: AUTH_CODE_VALIDITY_TIME,
            authCodeVerified: false
        };
        this.logoHeight = new Animated.Value(LOGO_HEIGHT);
    }

    UNSAFE_componentWillMount() {
        this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    keyboardWillShow = (event) => {
        Animated.timing(this.logoHeight, {
            duration: event.duration,
            toValue: LOGO_HEIGHT_SMALL
        }).start();
    };

    keyboardWillHide = (event) => {
        Animated.timing(this.logoHeight, {
            duration: event.duration,
            toValue: LOGO_HEIGHT
        }).start();
    };

    secondsToTime = (receivedSeconds) => {
        const divisorForMin = receivedSeconds % (60 * 60);
        const minutes = Math.floor(divisorForMin / 60);
        const divisorForSec = divisorForMin % 60;
        const seconds = Math.ceil(divisorForSec);

        return {
            mm: ('0' + minutes).slice(-2),
            ss: ('0' + seconds).slice(-2)
        }
    };

    startTimer = () => {
        if (this.timer === 0 && this.state.remainingTime > 0) {
            this.timer = setInterval(this.countDown, 1000);
        }
    };

    resetTimer = (callback) => {
        clearInterval(this.timer);
        this.timer = 0;
        this.setState({
            authCodeVerified: false,
            isSentAuthCode: false,
            remainingTime: AUTH_CODE_VALIDITY_TIME,
            time: this.secondsToTime(AUTH_CODE_VALIDITY_TIME)
        }, () => {
            if (typeof callback === 'function') {
                callback();
            }
        });
    };

    countDown = () => {
        let remainingTime = this.state.remainingTime - 1;

        this.setState({
            time: this.secondsToTime(remainingTime),
            remainingTime,
        });
        if (remainingTime === 0) {
            this.resetTimer();
        }
    };

    auth = (authCode) => {
        if (authCode === '0000') {
            this.setState({ authCodeVerified: true })
        } else {
            this.setState({ authCodeVerified: false })
        }
    };


    render() {
        const imgAuth = require('../../../images/auth.png');
        const imgAuthFail = require('../../../images/auth_fail.png');
        const imgAuthSuccess = require('../../../images/auth_success.png');
        const imgLoginLogo = require('../../../images/login_logo.png');
        const imgPhone = require('../../../images/phone.png');

        const {
            authCode,
            authCodeVerified,
            isAlreadySentAuthCode,
            isSentAuthCode,
            phoneNum,
            time
        } = this.state;

        const { navigation } = this.props;

        return (
            <Fragment>
                <StatusBar
                    backgroundColor="#ffffff"
                    barStyle="dark-content"
                />
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <SafeAreaView
                        style={styles.rootLayout}
                    >
                        <KeyboardAvoidingView
                            behavior={Platform.OS === "ios" ? "padding" : false}
                            enabled
                        >
                            <View style={styles.logoLayout}>
                                <Animated.Image
                                    style={[styles.logo, { width: this.logoHeight, height: this.logoHeight }]}
                                    source={imgLoginLogo}/>
                            </View>
                            <View style={styles.formLayout}>
                                <View style={styles.inputLayout}>
                                    <Image style={styles.inputIcon} source={imgPhone}/>
                                    <TextInput
                                        autoCorrect={false}
                                        autoFocus={true}
                                        keyboardType={"number-pad"}
                                        numberOfLines={1}
                                        maxLength={11}
                                        onChangeText={(phoneNum) => this.setState({ phoneNum })}
                                        placeholder={"휴대폰 번호"}
                                        selectionColor={"#dedede"}
                                        style={styles.inputText}
                                        underlineColorAndroid={"transparent"}
                                        value={phoneNum}
                                    />
                                    <TouchableOpacity
                                        style={[styles.inputButton,
                                            isAlreadySentAuthCode ? { width: 62 } : {},
                                            phoneNum.match(/^\d{11}$/) ? { borderColor: colors.brand500 } : { borderColor: colors.gray300 }]}
                                        onPress={() => {
                                            if (phoneNum.match(/^\d{11}$/)) {
                                                this.timer === 0
                                                    ? this.setState({
                                                        isSentAuthCode: true,
                                                        isAlreadySentAuthCode: true,
                                                    }, () => this.startTimer())
                                                    : () => {
                                                        this.resetTimer(this.startTimer)
                                                    };
                                                Keyboard.dismiss();
                                            }
                                        }}
                                    >
                                        <Text style={[styles.inputButtonText,
                                            phoneNum.match(/^\d{11}$/) ? { color: colors.brand500 } : { color: colors.gray300 }]}
                                        >
                                            {isAlreadySentAuthCode ? "재전송" : "인증번호 전송"}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.inputLayout}>
                                    <Image style={styles.inputIcon} source={imgAuth}/>
                                    <TextInput
                                        autoCorrect={false}
                                        autoFocus={false}
                                        keyboardType={"number-pad"}
                                        numberOfLines={1}
                                        maxLength={4}
                                        onChangeTexzt={(authCode) => {
                                            this.setState({ authCode }, () => {
                                                this.auth(authCode)
                                            })
                                        }}
                                        style={styles.inputText}
                                        placeholder={"인증번호"}
                                        value={authCode}
                                    />
                                    {
                                        isSentAuthCode
                                            ? <View style={styles.timerLayout}>
                                                {
                                                    authCode.length === 4
                                                        ? <Image
                                                            style={styles.authIcon}
                                                            source={authCodeVerified ? imgAuthSuccess : imgAuthFail}
                                                        />
                                                        : <View/>
                                                }
                                                {
                                                    authCodeVerified
                                                        ? <View/>
                                                        : <Text style={styles.timerText}>
                                                            {time.mm + ":" + time.ss}
                                                        </Text>
                                                }
                                            </View>
                                            : <Text style={{ width: 0 }}/>
                                    }
                                </View>
                                <View stlye={styles.buttonLayout}>
                                    <FlatButton
                                        active={authCodeVerified}
                                        onPress={() => {
                                            navigation.navigate(ScreenNames.MAIN)
                                        }}
                                        text={"로그인"}
                                    />
                                </View>
                                <View style={styles.viewSubButtons}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            navigation.navigate(ScreenNames.SIGN_UP)
                                        }}
                                    >
                                        <Text style={styles.textSubButton}>{"회원가입"}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Text style={styles.textSubButton}>{"휴대폰 번호가 바뀌었어요."}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </KeyboardAvoidingView>
                    </SafeAreaView>
                </TouchableWithoutFeedback>
            </Fragment>
        );
    }
}

export default LoginScreen;