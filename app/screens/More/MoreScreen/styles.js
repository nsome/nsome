import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    viewRoot: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 38,
    }
});

export default styles;