import React from 'react';
import {
    FlatList,
    Text,
    View
} from 'react-native';
import {
    ItemMore,
    MainHeaderLeft,
    MainHeaderRight
} from '../../../components';
import * as ScreenNames from '../../ScreenNames';
import styles from './styles';

const list = [
    { text: '공지사항', routeName: ScreenNames.NOTICE },
    { text: '이벤트', routeName: ScreenNames.EVENT },
    { text: '이용약관', routeName: ScreenNames.TERMS_AND_CONDITIONS },
    { text: '이메일 문의', routeName: ScreenNames.INQUIRY },
    { text: '아는사람 피하기', routeName: ScreenNames.AVOID_ACQUAINTANCE },
    { text: '설정', routeName: ScreenNames.SETTING },
];


class MoreScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: "더보기",
            headerLeft: (
                <MainHeaderLeft
                    beanNum={10}
                    navigation={navigation}
                />
            ),
            headerRight: (
                <MainHeaderRight
                    navigation={navigation}
                />
            )
        }
    };

    _keyExtractor = (item, index) => index;

    render() {

        const { navigation } = this.props;

        return (
            <View style={styles.viewRoot}>
                {
                    list.map((item, index) => {
                        return (
                            <ItemMore
                                key={index}
                                text={item.text}
                                onPress={() => navigation.navigate(item.routeName)}
                            />
                        )
                    })
                }
            </View>
        )
    }
}

export default MoreScreen;