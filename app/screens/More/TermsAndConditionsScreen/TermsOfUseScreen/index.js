import React from 'react';
import {
    Text,
    View
} from 'react-native';


class TermsOfUseScreen extends React.Component {
    render() {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Text>TermsOfUseScreen</Text>
            </View>
        )
    }
}

export default TermsOfUseScreen;