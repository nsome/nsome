import React from 'react';
import {
    FlatList, Text,
    View
} from 'react-native';
import { HeaderBack } from '../../../components';
import colors from '../../../colors';
import styles from '../../../components/ItemPost/styles';


class NoticeDetailScreen extends React.Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            title: "공지사항",
            headerLeft: (
                <HeaderBack onPress={() => navigation.goBack()}/>
            ),
            headerStyle: {
                ...navigationOptions.headerStyle,
                borderBottomWidth: 1,
                borderBottomColor: colors.$gray100
            }
        }
    };

    render() {
        const { title, date } = this.props;
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: colors.$gray50
                }}
            >
                <View>
                    <Text
                        style={styles.textTitle}
                    >
                        {title}
                    </Text>
                    <Text style={styles.textDate}>
                        {date}
                    </Text>
                </View>
            </View>
        )
    }
}


export default NoticeDetailScreen;