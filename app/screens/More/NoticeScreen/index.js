import React from 'react';
import {
    FlatList,
    View
} from 'react-native';
import {
    HeaderBack,
    ItemPost,
} from '../../../components';
import colors from '../../../colors'
import styles from './styles'

const dummy = [
    {
        title: `불량유저 신고, 이렇게 하세요!`,
        date: `2019.05.12`
    },
    {
        title: `불량유저 신고, 이렇게 하세요! 불량유저 신고, 이렇게 하세요! 불량유저 신고, 이렇게 하세요!`,
        date: `2019.05.12`
    }
];

class NoticeScreen extends React.Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
            title: "공지사항",
            headerLeft: (
                <HeaderBack onPress={() => navigation.goBack()}/>
            ),
            headerStyle: {
                ...navigationOptions.headerStyle,
                borderBottomWidth: 1,
                borderBottomColor: colors.$gray100
            }
        }
    };

    _keyExtractor = (item, index) => index;

    _renderItem = ({ item }) => (
        <ItemPost
            title={item.title}
            date={item.date}
        />
    );

    render() {
        return (
            <View
                style={styles.viewRoot}
            >
                <FlatList
                    keyExtractor={this._keyExtractor}
                    data={dummy}
                    renderItem={this._renderItem}
                />
            </View>
        )
    }
}

export default NoticeScreen;