import React from 'react';
import {
    Animated,
    Dimensions,
    Image,
    ImageBackground,
    ScrollView,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {

} from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import {
    ImageSlider,
    ItemProfileMoreDetail,
} from '../../../components';
import styles from './styles';

const WIDTH = Dimensions.get('window').width;
const images = [
    'https://pds.joins.com/news/component/ilgan_isplus/201904/26/2019042607324882100.jpeg',
    'https://dimg.donga.com/wps/NEWS/IMAGE/2017/05/26/84581706.2.jpg',
    'https://newsimg.sedaily.com/2017/11/23/1ONOW553SS_1.jpg',
];


class OtherProfileScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            ...navigation,
        }
    };

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount(): void {
        this.props.navigation.setParams({

        })
    }

    render() {
        const { navigation } = this.props;
        const { user } = navigation.state.params;
        const { imgSrc, nickname, location, age, background } = user;
        const imgGuarantee = require('../../../images/guarantee_20.png');
        const imgSendCoffeeWhite = require('../../../images/sendcoffee_white.png');
        console.log(navigation);
        console.log(navigation.state);


        return (
            <View
                stlye={{
                    flex: 1,
                    height: '100%',
                }}
            >
                <ScrollView>
                    <View style={{ width: WIDTH, height: WIDTH }}>
                        <ImageSlider
                            style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}
                            entries={images}
                            size={WIDTH}
                        />
                        <LinearGradient
                            colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0.9)']}
                            style={[styles.linearGradient, { height: WIDTH / 3 }]}
                        >
                            <View style={styles.viewUserInfo}>
                                <View style={styles.viewName}>
                                    <Text style={styles.textNickname}>{nickname}</Text>
                                    <Image style={styles.imgGuarantee} source={imgGuarantee}/>
                                </View>
                                <View style={styles.viewInfo}>
                                    <Text style={styles.textInfo}>{`${location} | ${age}세`}</Text>
                                    <Text style={styles.textInfo}>{background}</Text>
                                </View>
                            </View>
                        </LinearGradient>
                    </View>
                    <View style={styles.viewSection}>
                        <Text style={styles.textTitle}>
                            {"반가워요! "}
                            <Text style={styles.textSelfIntroductionHelloName}>{nickname}</Text>
                            {"입니다."}
                        </Text>
                        <Text
                            style={styles.textSelfIntroduction}>{`커피 좋아하시는 분과 소소한 이야기들 나누고 싶어요. 맛있는 커피 내려드릴게요 :)`}</Text>
                    </View>
                    <View style={styles.viewDivider}/>
                    <View style={styles.viewSection}>
                        <Text style={styles.textTitle}>
                            {"저는 이런 사람이에요 :)"}
                        </Text>
                        <View style={styles.viewProfileDetail}>
                            <Text style={styles.textProfileDetailTitle}>
                                {"저는"}
                            </Text>
                            <Text style={styles.textProfileDetailDescribe}>
                                <Text style={styles.textProfileDetailDescribeEmphasize}>
                                    {"22세"}
                                </Text>
                                {"이고, "}
                                <Text style={styles.textextProfileDetailDescribeEmphasizet}>
                                    {"서울"}
                                </Text>
                                {"에 살아요."}
                            </Text>
                        </View>
                        <View style={styles.viewProfileDetail}>
                            <Text style={styles.textProfileDetailTitle}>
                                {"학교"}
                            </Text>
                            <Text style={styles.textProfileDetailDescribe}>
                                <Text style={styles.textProfileDetailDescribeEmphasize}>
                                    {"서울종합예술대학교"}
                                </Text>
                                {"에서 공부했어요."}
                            </Text>
                        </View>
                        <View style={styles.viewProfileDetail}>
                            <Text style={styles.textProfileDetailTitle}>
                                {"신체"}
                            </Text>
                            <Text style={styles.textProfileDetailDescribe}>
                                {"키는 "}
                                <Text style={styles.textProfileDetailDescribeEmphasize}>
                                    {"185cm"}
                                </Text>
                                {"이고, "}
                                <Text style={styles.textextProfileDetailDescribeEmphasizet}>
                                    {"슬림 탄탄한 체형"}
                                </Text>
                                {"이에요."}
                            </Text>
                        </View>
                        <View style={styles.viewProfileDetail}>
                            <Text style={styles.textProfileDetailTitle}>
                                {"종교"}
                            </Text>
                            <Text style={styles.textProfileDetailDescribe}>
                                {"종교는 없습니다."}
                            </Text>
                        </View>
                        <View style={styles.viewProfileDetail}>
                            <Text style={styles.textProfileDetailTitle}>
                                {"성격"}
                            </Text>
                            <Text style={styles.textProfileDetailDescribe}>
                                <Text style={styles.textProfileDetailDescribeEmphasize}>
                                    {"차분하고, 듬직하고, 조용한"}
                                </Text>
                                {" 성격이에요."}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.viewDivider}/>
                    <View style={styles.viewSection}>
                        <Text style={styles.textTitle}>
                            {"제 이야기를 더 해볼게요!"}
                        </Text>
                        <View style={styles.viewProfileMoreDetail}>
                            <Text style={styles.textProfileDetailTitle}>
                                {"저의 매력은"}
                            </Text>
                            <View style={styles.viewProfileMoreDetailContainer}>
                                <ItemProfileMoreDetail text={"패션 감각"}/>
                                <ItemProfileMoreDetail text={"큰 눈"}/>
                                <ItemProfileMoreDetail text={"배려심이 깊어요"}/>
                                <ItemProfileMoreDetail text={"요리를 잘해요"}/>
                                <ItemProfileMoreDetail text={"예의가 발라요"}/>
                                <ItemProfileMoreDetail text={"보조개"}/>
                            </View>
                        </View>
                        <View style={styles.viewProfileMoreDetail}>
                            <Text style={styles.textProfileDetailTitle}>
                                {"저의 관심사는"}
                            </Text>
                            <View style={styles.viewProfileMoreDetailContainer}>
                                <ItemProfileMoreDetail text={"패션 감각"}/>
                                <ItemProfileMoreDetail text={"큰 눈"}/>
                                <ItemProfileMoreDetail text={"배려심이 깊어요"}/>
                            </View>
                        </View>
                        <View style={styles.viewProfileMoreDetail}>
                            <Text style={styles.textProfileDetailTitle}>
                                {"저의 데이트 스타일은"}
                            </Text>
                            <View style={styles.viewProfileMoreDetailContainer}>
                                <ItemProfileMoreDetail text={"요리를 잘해요"}/>
                                <ItemProfileMoreDetail text={"예의가 발라요"}/>
                                <ItemProfileMoreDetail text={"보조개"}/>
                            </View>
                        </View>
                        <View style={styles.viewProfileMoreDetail}>
                            <Text style={styles.textProfileDetailTitle}>
                                {"제가 찾는분은"}
                            </Text>
                            <View style={styles.viewProfileMoreDetailContainer}>
                                <ItemProfileMoreDetail text={"패션 감각"}/>
                                <ItemProfileMoreDetail text={"큰 눈"}/>
                                <ItemProfileMoreDetail text={"배려심이 깊어요"}/>
                                <ItemProfileMoreDetail text={"요리를 잘해요"}/>
                                <ItemProfileMoreDetail text={"예의가 발라요"}/>
                                <ItemProfileMoreDetail text={"보조개"}/>
                            </View>
                        </View>
                    </View>
                    <View style={styles.viewDivider}/>
                    <View style={styles.viewSection}>
                    </View>
                </ScrollView>
                <TouchableOpacity style={styles.viewRequestDrinkCoffee}>
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={['#00d0f3f2', '#00e1e4f2']}
                        style={styles.viewRequestDrinkCoffeeGradient}
                    >
                        <Text style={styles.textRequestCoffee}>{`커피 한잔할까요?`}</Text>
                        <View style={styles.viewRequestCoffeeBean}>
                            <Image
                                style={styles.imgRequestCoffee}
                                source={imgSendCoffeeWhite}
                            />
                            <Text style={styles.textRequestCoffeeBeanNum}>{`5+`}</Text>
                        </View>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        )
    }
}

export default OtherProfileScreen;