import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    linearGradient: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end'
    },
    viewUserInfo: {
        marginStart: 18,
        marginBottom: 18,
    },
    viewName: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    textNickname: {
        color: '$gray0',
        fontSize: 16,
        fontWeight: 'bold'
    },
    imgGuarantee: {
        width: 20,
        height: 20,
        marginStart: 4,
    },
    viewInfo: {
        marginTop: 7,
    },
    textInfo: {
        color: '$gray0',
        fontSize: 13,
    },
    viewSection: {
        padding: 20,
    },
    viewProfileDetail: {
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 14,
    },
    viewProfileMoreDetail: {
        flexDirection: 'column',
    },
    viewProfileMoreDetailContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    textProfileDetailTitle: {
        color: '$gray400',
        fontSize: 12,
    },
    textProfileDetailDescribe: {
        color: '$gray800',
        fontSize: 14,
        marginStart: 20,
        marginEnd: 20,
    },
    textProfileDetailDescribeEmphasize: {
        color: '$brand500',
    },
    textTitle: {
        color: '$gray800',
        fontSize: 12,
        fontWeight: 'bold',
        marginBottom: 16,
    },
    textSelfIntroductionHelloName: {
        color: '$brand500'
    },
    textSelfIntroduction: {
        color: '$gray800',
        fontSize: 14,
        marginTop: 16,
    },
    viewDivider: {
        width: '100%',
        height: 9,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '$gray100',
        backgroundColor: '$gray50',
    },
    viewRequestDrinkCoffee: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        marginStart: 18,
        marginEnd: 18,
        elevation: 6,
        shadowColor: '#00cce4',
        shadowOffset: {
            width: 0,
            height: -6
        },
        shadowOpacity: 0.06,
    },
    viewRequestDrinkCoffeeGradient: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: 16,
        paddingBottom: 16,
        marginBottom: 20,
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textRequestCoffee: {
        color: '$gray0',
        fontSize: 16,
        fontWeight: 'bold',
    },
    viewRequestCoffeeBean: {
        position: 'absolute',
        top: 11,
        bottom: 11,
        right: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    imgRequestCoffee: {
        width: 26,
        height: 26,
    },
    textRequestCoffeeBeanNum: {
        color: '$gray0',
        fontSize: 16,
        fontWeight: 'bold',
    },
});

export default styles;