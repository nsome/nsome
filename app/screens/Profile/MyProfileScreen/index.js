import React from 'react';
import {
    Text,
    TouchableOpacity,
    View
} from 'react-native';

class MyProfileScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
        }

    };

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Text>내 프로필</Text>
            </View>
        )
    }
}

export default MyProfileScreen;