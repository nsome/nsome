import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    viewRoot: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '$gray50'
    },
    viewMyBeanInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingStart: 20,
        paddingEnd: 16,
        height: 44,
        backgroundColor: '$gray0',
        borderColor: '$gray100',
        borderBottomWidth: 1
    },
    textCurrentBean: {
        fontSize: 13,
        color: '$gray800'
    },
    textCurrentBeanNum: {
        fontSize: 13,
        color: '$brand500'
    }
});

export default styles