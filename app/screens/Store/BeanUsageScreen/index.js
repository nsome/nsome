import React from 'react';
import {
    FlatList,
    Text,
    View
} from 'react-native';
import { HeaderBack, ItemBeanUsage } from '../../../components';
import { NavigationActions } from "react-navigation";
import styles from './styles';

const dummy = [
    {
        date: '2018.12.01',
        detail: '출석 보상',
        beanNum: '1'
    },
    {
        date: '2018.12.01',
        detail: '오늘의 공통분모 보상',
        beanNum: '1'
    },
    {
        date: '2018.12.01',
        detail: '커피콩 보내기',
        beanNum: '5'
    },
    {
        date: '2018.12.01',
        detail: '커피콩 충전',
        beanNum: '200'
    }
];

class BeanUsageScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: "콩 사용내역",
            headerLeft: (
                <HeaderBack onPress={() => navigation.dispatch(NavigationActions.back())}/>
            ),
            headerRight: null
        }
    };

    _keyExtractor = (item, index) => index;

    _renderItem = ({ item }) => (
        <ItemBeanUsage
            date={item.date}
            detail={item.detail}
            beanNum={item.beanNum}
        />
    );

    render() {
        return (
            <View style={styles.viewRoot}>
                <View style={styles.viewMyBeanInfo}>
                    <Text style={styles.textCurrentBean}>
                        {"현재 보유 콩 "}
                        <Text style={styles.textCurrentBeanNum}>{5}</Text>
                        {"개"}
                    </Text>
                </View>
                <View
                    style={{
                        flex: 1,
                    }}
                >
                    <FlatList
                        keyExtractor={this._keyExtractor}
                        data={dummy}
                        renderItem={this._renderItem}
                    />
                </View>
            </View>
        )
    }
}

export default BeanUsageScreen;