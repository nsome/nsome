import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    rootLayout: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '$gray50'
    },
    myBeanInfoLayout: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingStart: 20,
        paddingEnd: 16,
        height: 44,
        backgroundColor: '$gray0',
        borderColor: '$gray100',
        borderBottomWidth: 1
    },
    currentBeanText: {
        fontSize: 13,
        color: '$gray800'
    },
    currentBeanCountText: {
        fontSize: 13,
        color: '$brand500'
    },
    detailBeanUsageText: {
        fontSize: 13,
        color: '$gray400'
    }
});

export default styles