import React from 'react';
import {
    FlatList,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {
    NavigationActions
} from 'react-navigation';
import styles from './styles';
import colors from '../../../colors';
import { HeaderClose, ItemStore, MainHeaderLeft, MainHeaderRight } from '../../../components';
import * as ScreenNames from '../../ScreenNames'

const dummy = [
    {
        beanNum: 20,
        bonusBeanNum: 0,
        price: "5,600"
    },
    {
        beanNum: 40,
        bonusBeanNum: 10,
        price: "11,200"
    },
    {
        beanNum: 80,
        bonusBeanNum: 20,
        price: "22,400"
    },
    {
        beanNum: 160,
        bonusBeanNum: 40,
        price: "44,800"
    },
    {
        beanNum: 320,
        bonusBeanNum: 60,
        price: "67,200"
    },
    {
        beanNum: 640,
        bonusBeanNum: 100,
        price: "179,000"
    }
];

class StoreScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: "스토어",
            headerLeft: null,
            headerRight: (
                <HeaderClose onPress={() => navigation.dispatch(NavigationActions.back())}/>
            )
        }
    };

    _keyExtractor = (item, index) => index;

    _renderItem = ({ item }) => (
        <ItemStore
            beanNum={item.beanNum}
            bonusBeanNum={item.bonusBeanNum}
            price={item.price}
        />
    );

    render() {
        const { navigation } = this.props;

        return (
            <View style={styles.rootLayout}>
                <View style={styles.myBeanInfoLayout}>
                    <Text style={styles.currentBeanText}>
                        {"현재 보유 콩 "}
                        <Text style={styles.currentBeanCountText}>{5}</Text>
                        {"개"}
                    </Text>
                    <TouchableOpacity onPress={() => navigation.push(ScreenNames.BEAN_USAGE)}>
                        <Text style={styles.detailBeanUsageText}>{"콩 사용내역 >"}</Text>
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        flex: 1,
                    }}
                >
                    <FlatList
                        keyExtractor={this._keyExtractor}
                        data={dummy}
                        renderItem={this._renderItem}
                    />
                </View>
            </View>
        );
    }
}

export default StoreScreen;