import React from 'react';
import { View, Text } from 'react-native';
import * as ScreenNames from '../../ScreenNames'


const LOADING_SECONDS = 1000;

export default class SplashScreen extends React.Component {
    componentDidMount(): void {
        setTimeout(() => {
            this.props.navigation.navigate(ScreenNames.LOGIN);
        }, LOADING_SECONDS);
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Text>Splash</Text>
            </View>
        )
    }
}