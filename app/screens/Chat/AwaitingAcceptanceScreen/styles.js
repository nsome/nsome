import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    viewRoot: {
        justifyContent: 'center',
        flex: 1,
        paddingTop: 14,
        paddingStart: 18,
        paddingEnd: 18,
        backgroundColor: '$gray50'
    }
});

export default styles;