import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    viewInput: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: '$gray50',
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imgPhoto: {
        width: 26,
        height: 26,
    },
    imgSend: {
        width: 28,
        height: 28,
    },
});

export default styles;