import React, { Component } from 'react';
import {
    Image,
    Keyboard,
    SafeAreaView,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import { HeaderBack, HeaderExit, ChatTextInput } from '../../../components';
import styles from './styles';
import colors from '../../../colors';

export default class ChatRoomScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('name', '이름'),
            headerLeft: (
                <HeaderBack onPress={() => navigation.goBack()}/>
            ),
            headerRight: (
                <HeaderExit onPress={() => {
                }}/>
            ),
            headerStyle: {
                height: 56,
                borderBottomWidth: 0,
                backgroundColor: 'rgba(247, 247, 247, 0.9)',
                borderBottomColor: colors.$gray100,
                elevation: 0,
            }
        }

    };

    constructor(props) {
        super(props);
        this.state = {
            messageText: '',
        }
    }

    onChangedText(messageText) {
        this.setState({ messageText })
    }

    render() {
        const { messageText } = this.state;

        return (
            <SafeAreaView
                forceInset={{ bottom: 'always' }}
                style={{
                    flex: 1
                }}
                onPress={() => Keyboard.dismiss()}
            >
                <View style={{
                    flex: 1,
                    backgroundColor: colors.$gray50}}
                >
                    <ScrollView contentContainerStyle={{
                        flexGrow: 1,
                        top: 0,
                        bottom: 60
                    }}>
                        <Text>{"dsfasfasf"}</Text>
                    </ScrollView>
                    <View style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        bottom: 0,
                    }}>
                        <ChatTextInput
                            backgroundColor={colors.$gray0}
                            onChange={(msg) => this.onChangedText(msg)}
                            placeholder={"메시지를 입력해주세요."}
                            placeholderTextColor={colors.$gray300}
                            selectionColor={colors.$brand500}
                            multiline={true}
                            messageText={messageText}
                        />
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}