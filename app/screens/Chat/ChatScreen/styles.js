import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    viewRoot: {
        flex: 1,
        backgroundColor: '$gray50'
    },
    viewAwaiting: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    viewChatRooms: {
        flex: 1,
        marginTop: 12,
    },
    textAwaiting: {
        color: '$gray400',
        fontSize: 11
    },
    textAwaitingNum: {
        color: '$gray800'
    },
    imageAwaiting: {
        marginStart: 2,
        width: 12,
        height: 12
    }
});

export default styles;