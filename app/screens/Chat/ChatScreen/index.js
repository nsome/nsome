import React from 'react';
import {
    FlatList,
    Image,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { HeaderClose, ItemChatRoom } from '../../../components';
import styles from './styles';
import * as ScreenNames from '../../../screens/ScreenNames';

const dummy = [
    {
        other: {
            profileImage: "https://facebook.github.io/react/logo-og.png",
            name: '초내대신'
        },
        lastMessage: "매칭 성공을 축하드립니다!\n지금 대화를 시작해보세요 :)",
        unreadMessageNum: 8,
        date: '방금 전'
    },
    {
        other: {
            profileImage: "https://facebook.github.io/react/logo-og.png",
            name: '차차'
        },
        lastMessage: "날씨도 많이 선선해졌는데 우리 한강으로 피크닉 갈까요?",
        unreadMessageNum: 99,
        date: '8월 24일'
    },
    {
        other: {
            profileImage: "https://facebook.github.io/react/logo-og.png",
            name: '차차'
        },
        lastMessage: "날씨도 많이 선선해졌는데 우리 한강으로 피크닉 갈까요?",
        unreadMessageNum: 103,
        date: '8월 24일'
    }
];

class ChatScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: "메시지",
            headerLeft: null,
            headerRight: (
                <HeaderClose onPress={() => navigation.dispatch(NavigationActions.back())}/>
            )
        }

    };

    _keyExtractor = (item, index) => index;

    _renderItem = ({ item }) => (
        <ItemChatRoom
            date={item.date}
            lastMessage={item.lastMessage}
            navigation={this.props.navigation}
            other={item.other}
            unreadMessageNum={item.unreadMessageNum}
        />
    );

    render() {
        const { navigation } = this.props;
        const imgChatMore = require('../../../images/chat_more.png');


        return (
            <View style={styles.viewRoot}>
                <TouchableOpacity
                    style={styles.viewAwaiting}
                    onPress={() => navigation.navigate(ScreenNames.AWAITING_ACCEPTANCE)}
                >
                    <Text style={styles.textAwaiting}>
                        {"상대의 수락 대기 중 "}
                        <Text style={styles.textAwaitingNum}>{0}</Text>
                        {"건"}
                    </Text>
                    <Image source={imgChatMore} style={styles.imageAwaiting} />
                </TouchableOpacity>
                <View style={styles.viewChatRooms}>
                    <FlatList
                        keyExtractor={this._keyExtractor}
                        data={dummy}
                        renderItem={this._renderItem}
                    />
                </View>
            </View>
        );
    }
};

export default ChatScreen;