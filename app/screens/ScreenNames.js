// Chat
export const CHAT = "CHAT";
export const AWAITING_ACCEPTANCE = "AWAITING_ACCEPTANCE";
export const CHAT_ROOM = "CHAT_ROOM";

// Login
export const LOGIN = "LOGIN";

// Splash
export const SPLASH = "SPLASH";

// Main
export const MAIN = "MAIN";
export const MAIN_LOUNGE = "MAIN_LOUNGE";
export const MAIN_MATCHING = "MAIN_MATCHING";
export const MAIN_QUIZ = "MAIN_QUIZ";
export const MAIN_RATING = "MAIN_RATING";
export const MAIN_SETTING = "MAIN_SETTING";

// SignUp
export const SIGN_UP = "SIGN_UP";
export const SIGN_UP_PHONE_AUTH = "SIGN_UP_PHONE_AUTH";
export const SIGN_UP_SEX_SELECTION = "SIGN_UP_SEX_SELECTION";

// More
export const AVOID_ACQUAINTANCE = "AVOID_ACQUAINTANCE";
export const EVENT = "EVENT";
export const INQUIRY = "INQUIRY";
export const MORE = "MORE";
export const NOTICE = "NOTICE";
export const SETTING = "SETTING";
export const TERMS_AND_CONDITIONS = "TERMS_AND_CONDITIONS";
export const TERMS_OF_USE = "TERMS_OF_USE";
export const PRIVACY_POLICY = "PRIVACY_POLICY";

// Store
export const STORE = "STORE";
export const BEAN_USAGE = "BEAN_USAGE";

// Profile
export const MY_PROFILE = "MY_PROFILE";
export const OTHER_PROFILE = "OTHER_PROFILE";