import React from 'react';
import {
    Image,
    TouchableOpacity
} from 'react-native';
import styles from './styles';

const HeaderBack = ({ onPress }) => {
    const imgBtnBack = require('../../images/btn_back.png');

    return (
        <TouchableOpacity
            style={styles.rootLayout}
            onPress={onPress}
        >
            <Image
                style={styles.imgBtn}
                source={imgBtnBack}
            />
        </TouchableOpacity>
    );
};

export default HeaderBack;