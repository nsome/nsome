import React from 'react';
import {
    Image,
    TouchableOpacity
} from 'react-native';
import styles from './styles';

const HeaderClose = ({ white = false, onPress }) => {
    let imgBtnClose;

    if (white) {
        imgBtnClose = require('../../images/close_white.png');
    } else {
        imgBtnClose = require('../../images/btn_close.png');
    }

    return (
        <TouchableOpacity
            style={styles.rootLayout}
            onPress={onPress}
        >
            <Image
                style={styles.imgBtn}
                source={imgBtnClose}
            />
        </TouchableOpacity>
    );
};

export default HeaderClose;