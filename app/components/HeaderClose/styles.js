import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    rootLayout: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    imgBtn: {
        width: 48,
        height: 48,
        marginStart: 4
    }
});

export default styles