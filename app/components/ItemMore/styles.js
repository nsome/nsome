import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    rootLayout: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: '100%',
        height: 60,
    },
    textLayout: {
        flex: 1,
        textAlignVertical: 'center',
        marginStart: 46,
        color: '$gray800',
        fontSize: 15,
        textAlign: 'left'
    }
});

export default styles;