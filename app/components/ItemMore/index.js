import React from 'react';
import {
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import styles from './styles';

const ItemSetting = ({ onPress, text }) => {
    return (
        <TouchableOpacity
            style={styles.rootLayout}
            onPress={onPress}
        >
            <View>
                <Text
                    style={styles.textLayout}
                >
                    {text}
                </Text>
            </View>
        </TouchableOpacity>
    )
};

export default ItemSetting;