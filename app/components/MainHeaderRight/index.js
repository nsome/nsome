import React from 'react';
import {
    Image,
    TouchableOpacity,
    View
} from 'react-native';
import styles from './styles';
import { NavigationActions } from 'react-navigation';
import * as ScreenNames from '../../screens/ScreenNames';

const MainHeaderRight = ({ navigation }) => {
    const imgBtnChat = require('../../images/btn_chat.png');
    const imgProfile = require('../../images/dump.jpg');

    return (
        <View
            style={styles.rootLayout}
        >
            <TouchableOpacity
                onPress={() => navigation.navigate('ChatStack', {}, NavigationActions.navigate({ routeName: ScreenNames.CHAT }))}
            >
                <Image
                    style={styles.imgChat}
                    source={imgBtnChat}
                />
            </TouchableOpacity>
            <TouchableOpacity>
                <Image
                    style={styles.imgProfile}
                    source={imgProfile}
                />
            </TouchableOpacity>
        </View>
    );
};

export default MainHeaderRight;