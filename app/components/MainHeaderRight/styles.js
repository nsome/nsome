import React from 'react';
import { StyleSheet } from 'react-native';
import colors from '../../colors'

const styles = StyleSheet.create({
    rootLayout: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    imgChat: {
        width: 34,
        height: 34,
        marginEnd: 10
    },
    imgProfile: {
        width: 34,
        height: 34,
        marginEnd: 15,
        borderRadius: 17
    }
});

export default styles