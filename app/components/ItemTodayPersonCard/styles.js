import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    viewRoot: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 20,
    },
    imgPhoto: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        resizeMode: 'contain',
        borderRadius: 4,
    },
    viewName: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 14,
        marginStart: 6,
    },
    textNickname: {
        color: '$gray800',
        fontSize: 14,
    },
    imgGuarantee: {
        width: 16,
        height: 16,
        marginStart: 2,
    },
    viewInfo: {
        marginTop: 4,
        marginStart: 6
    },
    textInfo: {
        color: '$gray400',
        fontSize: 12,
    }
});

export default styles;