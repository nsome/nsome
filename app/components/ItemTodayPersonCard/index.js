import React from 'react';
import {
    Image,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import styles from './styles';

const ItemTodayPersonCard = ({ indexInRow, itemWidth, user, onClick }) => {

    const concatenateInfo = (location, age, background) => {
        return `${location} | ${age}세 | ${background}`
    };

    const imgGuarantee = require('../../images/guarantee_16.png');
    const { imgSrc, nickname, location, age, background } = user;
    const viewRootStyle = {
        width: itemWidth,
    };

    switch (indexInRow) {
        case 0:
            viewRootStyle.paddingEnd = itemWidth * 0.037;
            break;
        case 1:
            viewRootStyle.paddingStart = itemWidth * 0.037;
            break;
    }

    return (
        <TouchableOpacity
            style={[styles.viewRoot, { ...viewRootStyle }]}
            onPress={onClick}
        >
            <View>
                <View style={{ width: itemWidth * 0.963, height: itemWidth * 0.963 }}>
                    <Image
                        style={styles.imgPhoto}
                        source={{
                            uri: imgSrc
                        }}
                    />
                </View>
                <View style={styles.viewName}>
                    <Text style={styles.textNickname}>{nickname}</Text>
                    <Image style={styles.imgGuarantee} source={imgGuarantee}/>
                </View>
                <View style={styles.viewInfo}>
                    <Text style={styles.textInfo}>
                        {concatenateInfo(location, age, background)}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default ItemTodayPersonCard;