import React from 'react';
import {
    StyleSheet,
    Image,
    View,
    TextInput
} from 'react-native';
import styles from './styles';

const ImageTextInput = ({ autoFocus, children, hint, icon, keyboardType, maxLength, onChangeText, value }) => {
    const textInputStyles = {};
    textInputStyles['paddingRight'] = children ? children.props.style.width + 14 : 14;

    return (
        <View style={styles.rootLayout}>
            <Image style={styles.icon} source={icon}/>
            <TextInput
                autoCorrect={false}
                autoFocus={autoFocus}
                keyboardType={keyboardType}
                numberOfLines={1}
                maxLength={maxLength}
                onChangeText={onChangeText}
                placeholder={hint}
                selectionColor={'#dedede'}
                style={[styles.textInput, textInputStyles]}
                underlineColorAndroid={'transparent'}
                value={value}
            />
            {
                <View style={styles.button}>{children}</View>
            }
        </View>
    )
};

export default ImageTextInput;