import React from 'react';
import { StyleSheet } from 'react-native';
import colors from '../../colors'

export const styles = StyleSheet.create({
    rootLayout: {
        flexDirection: 'row',
        borderBottomColor: colors.gray100,
        borderBottomWidth: 1,
        width: '100%',
        height: 52,
        marginBottom: 8,
        alignItems: 'center',
    },
    icon: {
        resizeMode: 'contain',
        position: 'absolute',
        left: 4,
        width: 24
    },
    textInput: {
        width: '100%',
        paddingLeft: 42,
        borderBottomWidth: 0,
        fontSize: 15
    },
    button: {
        position: 'absolute',
        right: 4
    }
});

export default styles;