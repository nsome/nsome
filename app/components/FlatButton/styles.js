import React from 'react';
import { Platform } from 'react-native';
import {
    horizontalScale,
    moderateScale,
    verticalScale
} from '../../utils/Scaling';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    rootView: {
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        width: 310,
        height: 52,
        borderRadius: 6
    },
    activeRootView: {
        backgroundColor: '$brand500',
    },
    inactiveRootView: {
        backgroundColor: '$gray150'
    },
    textActive: {
        color: '$gray0'
    },
    textInactive: {
        color: '$gray300'
    },
    text: {
        fontSize: 16,
    }
});

export default styles;