import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import styles from './styles';
import colors from '../../colors';

const FlatButton = ({ active, text, onPress }) => {


    return (
        <TouchableOpacity
            style={[
                styles.rootView,
                active ? styles.activeRootView : styles.inactiveRootView
            ]}
            onPress={onPress}
        >
            <Text
                style={[
                    styles.text,
                    active ? styles.textActive : styles.textInactive
                ]}
            >
                {text}
            </Text>
        </TouchableOpacity>
    )
};

export default FlatButton;