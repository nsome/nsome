import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    rootLayout: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: '100%',
        paddingStart: 20,
        paddingEnd: 20,
        paddingTop: 16,
        paddingBottom: 13,
        backgroundColor: '$gray0',
        borderBottomWidth: 1,
        borderBottomColor: '$gray100'
    },
    textTitle: {
        color: '$gray800',
        fontSize: 14,
        marginBottom: 5
    },
    textDate: {
        color: '$gray400',
        fontSize: 12,
    }
});

export default styles;