import React from 'react';
import {
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import styles from './styles';

const ItemPost = ({ onPress, title, date }) => {
    return (
        <TouchableOpacity
            style={styles.rootLayout}
            onPress={onPress}
        >
            <View>
                <Text
                    style={styles.textTitle}
                    numberOfLines={2}
                >
                    {title}
                </Text>
                <Text style={styles.textDate}>
                    {date}
                </Text>
            </View>
        </TouchableOpacity>
    )
};

export default ItemPost;