import React from 'react';
import {
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import styles from './styles';

const ItemStore = ({ onPress, beanNum, bonusBeanNum, price, isHot }) => {
    return (
        <TouchableOpacity
            style={styles.rootLayout}
            onPress={onPress}
        >
            <View>
                <Text>{beanNum}</Text>
            </View>
            <View>
                <Text>{`${price}원`}</Text>
            </View>
        </TouchableOpacity>
    )
};

export default ItemStore;