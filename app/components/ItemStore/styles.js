import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    rootLayout: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        paddingStart: 16,
        paddingEnd: 20,
        paddingTop: 18,
        paddingBottom: 18,
        backgroundColor: '$gray0',
        borderBottomWidth: 1,
        borderBottomColor: '$gray100'
    },
    textBeanNum: {
        color: '$gray800',
        fontSize: 14,
        marginBottom: 5
    },
    textPrice: {
        color: '$gray800',
        fontSize: 14,
    }
});

export default styles;