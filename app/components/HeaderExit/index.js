import React from 'react';
import {
    Image,
    TouchableOpacity
} from 'react-native';
import styles from './styles';

const HeaderExit = ({ onPress }) => {
    const imgBtnClose = require('../../images/back_exit.png');

    return (
        <TouchableOpacity
            style={styles.rootLayout}
            onPress={onPress}
        >
            <Image
                style={styles.imgBtn}
                source={imgBtnClose}
            />
        </TouchableOpacity>
    );
};

export default HeaderExit;