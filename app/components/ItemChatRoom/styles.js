import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    viewRoot: {
        flex: 1,
        width: '90%',
        alignSelf: 'center',
        flexDirection: 'row',
        paddingTop: 15,
        paddingBottom: 13,
        paddingStart: 14,
        paddingEnd: 16,
        marginTop: 5,
        marginBottom: 5,
        backgroundColor: '$gray0',
        borderRadius: 6,
        shadowColor: '#d2d2d2',
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowOpacity: 0.5
    },
    viewProfileImage: {
        width: 54,
        height: 54,
        borderRadius: 27,
        marginEnd: 14,
    },
    imageProfile: {
        borderRadius: 27
    },
    viewChatRoomInfo: {
        flex: 1,
    },
    viewChatRoomInfoRowOne: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    viewChatRoomInfoRowTwo: {
        width: '100%',
        marginTop: 3,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textOtherName: {
        color: '$gray800',
        fontSize: 15
    },
    textDate: {
        color: '$gray400',
        fontSize: 10
    },
    textLastMessage: {
        marginTop: 3,
        width: '80%',
        color: '$gray400',
        fontSize: 12,
    },
    viewUnread: {
        height: 20,
        borderRadius: 20 / 2,
        backgroundColor: '$red500',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textUnreadNum: {
        color: '$gray0',
        fontSize: 12,
    }
});

export default styles;