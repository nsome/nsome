import React from 'react';
import {
    Image,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import styles from './styles';
import * as ScreenNames from '../../screens/ScreenNames';


const ItemChatRoom = ({ date, lastMessage, navigation, other, unreadMessageNum }) => {

    let viewUnreadWidth = 20;
    if (unreadMessageNum > 99) {
        viewUnreadWidth = 34;
    } else if (unreadMessageNum > 9) {
        viewUnreadWidth = 28;
    }

    return (
        <TouchableOpacity
            style={styles.viewRoot}
            onPress={() => navigation.navigate(ScreenNames.CHAT_ROOM, other)}
        >
            <View style={styles.viewProfileImage}>
                <Image
                    source={{
                        uri: other.profileImage,
                    }}
                    style={styles.imageProfile}
                />
            </View>
            <View style={styles.viewChatRoomInfo}>
                <View style={styles.viewChatRoomInfoRowOne}>
                    <Text style={styles.textOtherName}>{other.name}</Text>
                    <Text style={styles.textDate}>{date}</Text>
                </View>
                <View style={styles.viewChatRoomInfoRowTwo}>
                    <Text style={styles.textLastMessage} numberOfLines={2}>
                        {lastMessage}
                    </Text>
                    <View style={[styles.viewUnread, { width: viewUnreadWidth }]}>
                        <Text style={styles.textUnreadNum}>
                            {unreadMessageNum > 99 ? `99+` : unreadMessageNum}
                        </Text>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default ItemChatRoom;