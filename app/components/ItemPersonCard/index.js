import React from 'react';
import {
    Image,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import styles from './styles';

const ItemPersonCard = ({ indexInRow, itemWidth, user, isLocked, onClick }) => {

    const concatenateInfo = (location, age) => {
        return `${location} | ${age}세`
    };

    const imgLock = require('../../images/lock_mini.png');
    const imgGuarantee = require('../../images/guarantee_16.png');
    const { imgSrc, nickname, location, age, background } = user;
    const viewRootStyle = {
        width: itemWidth,
        marginBottom: itemWidth * 0.08
    };

    switch (indexInRow) {
        case 0:
            viewRootStyle.paddingEnd = itemWidth * 0.08;
            break;
        case 1:
            break;
        case 2:
            viewRootStyle.paddingStart = itemWidth * 0.08;
            break;
    }

    return (
        <TouchableOpacity
            style={[styles.viewRoot, { ...viewRootStyle }]}
            onPress={onClick}
        >
            <View>
                <View style={{ width: itemWidth * 0.92, height: itemWidth * 0.92 }}>
                    <Image
                        style={styles.imgPhoto}
                        source={{
                            uri: imgSrc
                        }}/>
                    {
                        isLocked ? <View style={styles.imgLockedPhoto}/> : null
                    }
                    <View style={styles.viewRemainingDays}>
                        <Text style={styles.textRemainingDays}>{"D-4"}</Text>
                    </View>
                    {
                        isLocked ? <Image
                            style={styles.imgLock}
                            source={imgLock}/> : null
                    }
                </View>
                <View style={styles.viewName}>
                    <Text style={styles.textNickname}>{nickname}</Text>
                    <Image style={styles.imgGuarantee} source={imgGuarantee}/>
                </View>
                <View style={styles.viewInfo}>
                    <Text style={styles.textInfo}>
                        {concatenateInfo(location, age)}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default ItemPersonCard;