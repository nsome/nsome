import EStyleSheet from 'react-native-extended-stylesheet';
import {
    horizontalScale
} from '../../utils/Scaling';

const styles = EStyleSheet.create({
    viewRoot: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 10,
    },
    imgPhoto: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        resizeMode: 'contain',
        borderRadius: 4,
    },
    imgLockedPhoto: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        borderRadius: 4,
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
    },
    imgLock: {
        position: 'absolute',
        width: 24,
        height: 24,
        right: 6,
        bottom: 6
    },
    viewRemainingDays: {
        position: 'absolute',
        top: 4,
        left: 4,
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        borderRadius: horizontalScale(2),
        width: horizontalScale(26),
        height: horizontalScale(16),
        alignItems: 'center',
        justifyContent: 'center'
    },
    textRemainingDays: {
        color: '$gray0',
        fontSize: 10
    },
    viewName: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        marginStart: 6,
    },
    textNickname: {
        color: '$gray800',
        fontSize: 13,
    },
    imgGuarantee: {
        width: 16,
        height: 16,
        marginStart: 4,
    },
    viewInfo: {
        marginTop: 1,
        marginStart: 6
    },
    textInfo: {
        color: '$gray400',
        fontSize: 12,
    }
});

export default styles;