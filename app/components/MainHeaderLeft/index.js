import React from 'react';
import {
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import {
    NavigationActions
} from 'react-navigation';
import styles from './styles';
import * as ScreenNames from '../../screens/ScreenNames'

const MainHeaderLeft = ({ beanNum = 0, navigation }) => {
    console.log(navigation);
    const imgBtnStore = require('../../images/btn_store.png');

    return (
        <TouchableOpacity
            style={styles.rootLayout}
            onPress={() => navigation.navigate('StoreStack', {}, NavigationActions.navigate({ routeName: ScreenNames.STORE }))}
        >
            <Image
                style={styles.imgBean}
                source={imgBtnStore}
            />
            <Text
                style={styles.txtBean}
            >
                {beanNum}
            </Text>
        </TouchableOpacity>
    );
};

export default MainHeaderLeft;