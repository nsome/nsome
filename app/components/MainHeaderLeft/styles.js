import React from 'react';
import { StyleSheet } from 'react-native';
import colors from '../../colors'

const styles = StyleSheet.create({
    rootLayout: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    imgBean: {
        width: 34,
        height: 34,
        marginStart: 10
    },
    txtBean: {
        fontSize: 18,
        color: colors.$brand500
    }
});

export default styles