import React from 'react';
import {
    Image,
    Platform,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';
import colors from '../../colors';
import Carousel, { Pagination } from 'react-native-snap-carousel';


class ImageSlider extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            activeSlide: 0,
        }
    }

    _renderItem = ({ item, index }) => {
        const { size } = this.props;

        return (
            <View style={{ width: size, height: size }}>
                <Image
                    source={{ uri: item }}
                    style={{ ...StyleSheet.absoluteFillObject, }}
                    resizeMode={'cover'}
                />
            </View>
        )
    };

    get pagination() {
        const { activeSlide } = this.state;
        const { entries } = this.props;

        if (Platform.OS === 'android') {
            const hasNotch = StatusBar.currentHeight > 24;
        } else {

        }

        return (
            <Pagination
                dotsLength={entries.length}
                activeDotIndex={activeSlide}
                containerStyle={{
                    position: 'absolute',
                    top: 20,
                    left: 0,
                    right: 0,
                }}
                dotStyle={{
                    width: 6,
                    height: 6,
                    borderRadius: 3,
                    backgroundColor: colors.$gray0
                }}
                inactiveDotStyle={{}}
                inactiveDotOpacity={0.4}
                inactiveDotScale={1}
            />
        );
    }

    render() {
        const { entries, size } = this.props;
        return (
            <View>
                <Carousel
                    data={this.props.entries}
                    renderItem={this._renderItem}
                    onSnapToItem={(index) => this.setState({ activeSlide: index })}
                    sliderWidth={size}
                    itemWidth={size}
                    inactiveSlideScale={1}
                />
                {this.pagination}
            </View>
        );
    }
};

export default ImageSlider;