import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    viewRoot: {
        flexDirection: 'row',
        padding: 11,
        alignItems: 'center',
        elevation: 6,
        shadowColor: '#f0f0f0',
        shadowOffset: {
            width: 0,
            height: -4
        },
        shadowOpacity: 0.5
    },
    viewTextInput: {
        flex: 1,
        justifyContent: 'center',
        marginStart: 4,
        marginEnd: 12,
    },
    viewImgPhoto: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 1,
        marginEnd: 11,
    },
    viewImgSend: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInput: {
        fontSize: 13,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        textAlign: 'left',
        paddingTop: 10,
        paddingLeft: 0,
        paddingRight: 0,
    }
});

export default styles;