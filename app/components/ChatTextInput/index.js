import React from 'react';
import {
    View,
    TextInput,
    Image,
    Animated,
    Keyboard,
    TouchableOpacity,
    Platform
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import styles from './styles';

class ChatTextInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            height: 0,
            keyboardOffset: new Animated.Value(0)
        }
    }

    componentDidMount(): void {
        this._keyboardWillShowSubscription = Keyboard.addListener("keyboardWillShow", e => this._keyboardWillShow(e));
        this._keyboardWillHideSubscription = Keyboard.addListener("keyboardWillHide", e => this._keyboardWillHide(e));
    }

    _keyboardWillShow(e) {
        Animated.spring(this.state.keyboardOffset, {
            toValue: DeviceInfo.getModel() === "iPhone X" ? e.endCoordinates.height - 34 : e.endCoordinates.height,
            friction: 8
        }).start();
    }

    _keyboardWillHide(e) {
        Animated.spring(this.state.keyboardOffset, {
            toValue: 0,
            friction: 8
        }).start();
    }

    render() {
        const imgPhoto = require('../../images/attach.png');
        const imgSend = require('../../images/send.png');
        const {
            backgroundColor,
            placeholder,
            placeholderTextColor,
            selectionColor,
            messageText,
            multiline,
            keyboardType,
            onChange
        } = this.props;

        return (
            <Animated.View style={{ marginBottom: this.state.keyboardOffset }}>
                <View style={[styles.viewRoot, {
                    backgroundColor
                }]}>
                    <TouchableOpacity
                        style={styles.viewImgPhoto}
                    >
                        <Image style={{ width: 26, height: 26 }} source={imgPhoto}/>
                    </TouchableOpacity>
                    <View style={styles.viewTextInput}>
                        <TextInput
                            placeholder={placeholder}
                            placeholderTextColor={placeholderTextColor}
                            selectionColor={selectionColor}
                            keyboardType={keyboardType}
                            value={messageText}
                            onChange={(event) => { event.target.value }}
                            onChangeText={editedText => {
                                onChange(editedText)
                            }}
                            multiline={multiline}
                            onContentSizeChange={(event) => this.setState({ height: event.nativeEvent.contentSize.height })}
                            style={[styles.textInput, {
                                height: Math.min(120, Math.max(35, this.state.height))
                            }]}
                        />
                    </View>
                    <TouchableOpacity
                        style={styles.viewImgSend}
                    >
                        <Image style={{ width: 28, height: 28 }} source={imgSend}/>
                    </TouchableOpacity>
                </View>
            </Animated.View>
        )
    }
}

export default ChatTextInput;