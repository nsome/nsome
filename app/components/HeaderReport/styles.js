import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    rootLayout: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    imgBtn: {
        width: 48,
        height: 48,
        marginStart: 4
    }
});

export default styles