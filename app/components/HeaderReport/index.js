import React from 'react';
import {
    Image,
    TouchableOpacity
} from 'react-native';
import styles from './styles';

const HeaderReport = ({ white = false, onPress }) => {
    let imgBtnReport;

    if (white) {
        imgBtnReport = require('../../images/police_white.png');
    } else {
        imgBtnReport = require('../../images/police.png');
    }

    return (
        <TouchableOpacity
            style={styles.rootLayout}
            onPress={onPress}
        >
            <Image
                style={styles.imgBtn}
                source={imgBtnReport}
            />
        </TouchableOpacity>
    );
};

export default HeaderReport;