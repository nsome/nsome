import React from 'react';
import {
    Text,
    View
} from 'react-native';
import styles from './styles';

const ItemBeanUsage = ({ date, detail, beanNum }) => (
    <View style={styles.viewRoot}>
        <View style={styles.viewStart}>
            <Text style={styles.textDate}>{date}</Text>
            <Text style={styles.textDetail}>{detail}</Text>
        </View>
        <View>
            <Text>{beanNum}</Text>
        </View>
    </View>
);

export default ItemBeanUsage;