import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
    viewRoot: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: 44,
        paddingStart: 20,
        paddingEnd: 20,
        backgroundColor: '$gray0',
        borderBottomWidth: 1,
        borderBottomColor: '$gray100'
    },
    viewStart: {
        flexDirection: 'row'
    },
    textDate: {
        color: '$gray400',
        fontSize: 13
    },
    textDetail: {
        marginStart: 20,
        color: '$gray800',
        fontSize: 13
    },
    textBeanNum: {
        color: '$gray800',
        fontSize: 13
    }
});

export default styles;