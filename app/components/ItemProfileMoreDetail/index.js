import React from 'react';
import {
    Text,
    View
} from 'react-native';
import styles from './styles';

const ItemProfileMoreDetail = ({ text }) => (
    <View style={styles.viewRoot}>
        <Text style={styles.textMoreDetail}>
            {text}
        </Text>
    </View>
);

export default ItemProfileMoreDetail;

