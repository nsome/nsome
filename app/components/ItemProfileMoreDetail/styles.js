import EStyleSheet from 'react-native-extended-stylesheet';

const styles =  EStyleSheet.create({
    viewRoot: {
        height: 30,
        marginTop: 8,
        marginEnd: 8,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '$gray800',
        borderRadius: 4,
    },
    textMoreDetail: {
        paddingStart: 10,
        paddingEnd: 10,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: '$gray800',
        fontSize: 13,
    }
});

export default styles;